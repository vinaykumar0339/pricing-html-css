const toggle = document.getElementById('toggle');
const prices = document.getElementById('prices');

toggle.addEventListener('change', (e) => {
    prices.classList.toggle('show-monthly');
})

const priceCards = document.getElementsByClassName('price-card');

for (let i=0; i < priceCards.length; i++) {
    priceCards[i].addEventListener('click', function (e) {
        const currentPriceCard = document.getElementsByClassName('price-card-primary');
        currentPriceCard[0].className = currentPriceCard[0].className.replace('price-card-primary', '');
        this.className += ' price-card-primary';
    })
}